<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/25/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modify User</title>
</head>
<body>
<%
    ////////////////
    List<User> users;
    if (session.getAttribute("user_list") != null) { // sprawdzamy czy w sesji znajduje się nasza lista
        // wyciągamy listę userów z sesji jeśli tam jest
        users = (List<User>) session.getAttribute("user_list");
    } else {
        // jeśli listy nie ma w sesji tworzymy nową listę
        users = new ArrayList<>();
    }
    //////////////////
    String user_id = request.getParameter("modified_id");
    User searched = null;

    if (user_id != null) {
        Long modifiedId = Long.parseLong(user_id);
        Iterator<User> it = users.iterator();
        // dopóki mam następne elementy
        while (it.hasNext()) {
            // przechodzę do następnego elementu
            User user = it.next();

            // jeśli obecny element to ten, który mam usunąć
            if (user.getId() == modifiedId) {
                searched = user;
                it.remove();
                break;
            }
        }
    }

    // ustawiamy parametry
    searched.setFirstName(request.getParameter("firstName"));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    searched.setGender(gender);
    searched.setLastName(request.getParameter("lastName"));
    searched.setAddress(request.getParameter("address"));
    searched.setHeight(Integer.parseInt(request.getParameter("height")));
    searched.setUsername(request.getParameter("username"));

    request.getParameterMap();
    //2018-01-01T01:00
    LocalDateTime birthDate = LocalDateTime.parse(
            request.getParameter("birthdate"),
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));

    searched.setBirthDate(birthDate);

    // dodajemy zmodyfikowanego użytkownika do listy
    users.add(searched);

    // zapisujemy listę do sesji
    session.setAttribute("user_list", users);

    // przekierowanie na listę użytkowników
    response.sendRedirect("user_list.jsp");
%>
</body>
</html>
