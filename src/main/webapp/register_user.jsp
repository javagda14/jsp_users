<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/25/18
  Time: 6:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Action Register User</title>
</head>
<body>
<%! int licznik = 0; %>
<%
    User u = new User();
    u.setId(licznik++); // 0, 1, 2...

    u.setFirstName(request.getParameter("firstName"));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    u.setGender(gender);
    u.setLastName(request.getParameter("lastName"));
    u.setAddress(request.getParameter("address"));
    u.setHeight(Integer.parseInt(request.getParameter("height")));
    u.setUsername(request.getParameter("username"));
    u.setJoinDate(LocalDateTime.now());

    //2018-01-01T01:00
    LocalDateTime birthDate = LocalDateTime.parse(
            request.getParameter("birthdate"),
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));

    u.setBirthDate(birthDate);

    //////////////////////////////////////////////

    // deklaracja listy user'ów - żeby mieć do niej dostęp po if'ie musimy ją zadeklarować przed nim.
    List<User> users;
    if (session.getAttribute("user_list") != null) { // sprawdzamy czy w sesji znajduje się nasza lista
        // wyciągamy listę userów z sesji jeśli tam jest
        users = (List<User>) session.getAttribute("user_list");
    } else {
        // jeśli listy nie ma w sesji tworzymy nową listę
        users = new ArrayList<>();
    }

    // dodanie użytkownika do listy
    users.add(u);

    // zapisujemy listę user'ów do sesji
    session.setAttribute("user_list", users);

    // przekierowanie na stronę z listą
    response.sendRedirect("user_list.jsp");
%>

</body>
</html>
