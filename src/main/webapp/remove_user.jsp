<%@ page import="java.util.ArrayList" %>
<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/25/18
  Time: 7:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remove User</title>
</head>
<body>
<%
    List<User> users;
    if (session.getAttribute("user_list") != null) { // sprawdzamy czy w sesji znajduje się nasza lista
        // wyciągamy listę userów z sesji jeśli tam jest
        users = (List<User>) session.getAttribute("user_list");
    } else {
        // jeśli listy nie ma w sesji tworzymy nową listę
        users = new ArrayList<>();
    }

    String removedUserId = request.getParameter("user_id");
    Long removedId = Long.parseLong(removedUserId);

    // tworzę iterator listy users
    Iterator<User> it = users.iterator();
    // dopóki mam następne elementy
    while (it.hasNext()) {
        // przechodzę do następnego elementu
        User user = it.next();

        // jeśli obecny element to ten, który mam usunąć
        if (user.getId() == removedId) {
            // to usuwam element
            it.remove();
            // i kończę obieg pętli
            break;
        }
    }

    // umieszczam listę z powrotem w sesji
    session.setAttribute("user_list", users);

    response.sendRedirect("user_list.jsp");
%>
</body>
</html>
